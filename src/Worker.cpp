#include <iostream>
#include <signal.h>
#include "Worker.h"
#include "Socket.h"
#include "Master.h"
#include "Router.h"
#include "Atomic.h"
#include "Log.h"
#include "Config.h"

NS_USING;

Worker::Worker(Master *master, int id):
	nId(id),
	pMaster(master),
	pEventLoop(NULL),
	nRouterReconnect(0),
	nRouterDelay(0)
{
	pProtocol = new ProtocolFastcgi();
}

Worker::~Worker()
{
	delete pEventLoop;
	delete pProtocol;
}

void Worker::createClient(int fd, const char *ip, int port)
{
	//创建session对像
	Session sess(nPid, fd);
	LOG("new client, fd=%d, ip=%s, port=%d, sid=%s", fd, ip, port, sess.getId());
	
	//辅助数据
	ClientData *pData = new ClientData();
	char szPort[8];
	int nPort = sprintf(szPort, "%d", port);
	pData->session = sess;
	pData->nrequest = 0;
	pProtocol->makeParam(pData->params, "REMOTE_ADDR", sizeof("REMOTE_ADDR") - 1, ip, strlen(ip));
	pProtocol->makeParam(pData->params, "REMOTE_PORT", sizeof("REMOTE_PORT") - 1, szPort, nPort);
	pProtocol->makeParam(pData->params, "SESSIONID", sizeof("SESSIONID") - 1, sess.getId(), SID_LENGTH);
	
	//创建连接对像
	Connection *client = new Connection(pEventLoop, fd);
	client->setIPAndPort(ip, port);
	client->setData(pData);
	client->setMessageHandler(EV_CB(this, Worker::onMessage));
	client->setCloseHandler(EV_CB(this, Worker::onClose));
	//client->setWriteCompleteHandler(EL_CB(this, Worker::onWriteComplete));
	
	arrClients[sess.getId()] = client;
	
	//计数更新
	pMaster->addClient(nId);
	
	//Router通知
	sendToRouter(ROUTER_MSG_CONN, SID_LENGTH, sess.getId(), 0, NULL);
	
	//backend通知
	Config *pConfig = Config::getInstance();
	if(pConfig->bEventConnect){
		Buffer *pParams = new Buffer(pData->params);
		pProtocol->makeParam(*pParams, "EVENT", sizeof("EVENT") - 1, "1", 1);
		createBackendRequest(client, NULL, pParams, true);
	}
	
	//lua通知
	if(pMaster->pScript && pMaster->pScript->hasConnectProc()){
		pMaster->pScript->procConnect(client);
	}
}

void Worker::closeClient(Connection *client)
{
	ClientData *pData = (ClientData*)client->getData();
	std::string sid = pData->session.getId();
	LOG("close client, fd=%d, sid=%s", client->getSocket().getFd(), sid.c_str());
	
	//router通知
	sendToRouter(ROUTER_MSG_CLOSE, SID_LENGTH, pData->session.getId(), 0, NULL);
	
	//后端通知
	Config *pConfig = Config::getInstance();
	if(pConfig->bEventClose){
		Buffer *pParams = new Buffer(pData->params);
		pProtocol->makeParam(*pParams, "EVENT", sizeof("EVENT") - 1, "2", 1);
		createBackendRequest(NULL, NULL, pParams, true);
	}
	
	//释放后端请求
	for(BackendList::const_iterator it = pData->backends.begin(); it != pData->backends.end(); ++it){
		Connection *backend = it->first;
		BackendRequest *request = (BackendRequest*)backend->getData();
		request->client = NULL;//大哥已死，小弟们自生自灭...
		backend->close();
	}
	
	//取消频道订阅
	for(ChannelSet::const_iterator it2 = pData->channels.begin(); it2 != pData->channels.end(); ++it2){
		const std::string &chname = it2->first;
		
		ChannelList::iterator cit = arrChannels.find(chname);
		if(cit != arrChannels.end()){
			cit->second.erase(client);
			if(cit->second.empty()){
				sendToRouter(ROUTER_MSG_CH_UNSUB, chname.size(), chname.c_str(), 0, NULL);
				arrChannels.erase(cit);
			}
		}
	}
	
	//计数更新
	pMaster->delClient(nId);
	
	//客户端列表更新
	arrClients.erase(sid);
	
	//lua更新
	if(pMaster->pScript && pMaster->pScript->hasCloseProc()){
		pMaster->pScript->procClose(client);
	}
	
	delete pData;
	delete client;
}

void Worker::sendToRouter(uint_16 type, uint_16 slen, const char *sessptr, int len, const char *dataptr)
{
	if(pRouter){
		RouterMsg msg;
		net::writeNetInt16((char*)&msg.type, type);
		net::writeNetInt16((char*)&msg.slen, slen);
		net::writeNetInt32((char*)&msg.len, len);
		pRouter->send((char*)&msg, ROUTER_HEAD_SIZE);
		if(slen){
			pRouter->send(sessptr, slen);
		}
		if(len){
			pRouter->send(dataptr, len);
		}
	}
}

void Worker::sendToClientRaw(Connection *conn, const char *data, int len)
{
	if(len){
		char hdr[4];
		net::writeNetInt32(hdr, len);
		conn->send(hdr, 4);
		conn->send(data, len);
	}
}

void Worker::sendToClientScript(Connection *conn, Buffer *msg)
{
	ClientData *pData = (ClientData*)conn->getData();
	Buffer resp;
	int ret = pMaster->pScript->procOutput(conn, pData->nrequest, msg, &resp);
	if(ret > 0){
		if(!resp.empty()){
			conn->send(resp.data(), resp.size());
		}
	}else if(ret == 0){
		if(!msg->empty()){
			sendToClientRaw(conn, msg->data(), msg->size());
		}
	}
}

void Worker::sendToClient(Connection *conn, Buffer *msg)
{
	if(pMaster->pScript && pMaster->pScript->hasOutputProc()){
		sendToClientScript(conn, msg);
	}else if(!msg->empty()){
		sendToClientRaw(conn, msg->data(), msg->size());
	}
}

void Worker::sendToClient(Connection *conn, const char *data, int len)
{
	if(pMaster->pScript && pMaster->pScript->hasOutputProc()){
		Buffer msg(data, len);
		sendToClientScript(conn, &msg);
	}else if(len){
		sendToClientRaw(conn, data, len);
	}
}

void Worker::onChannel(int fd, int ev, void *data)
{
	LOG("on channel");
	ChannelMsg ch;
	int n = recv(&ch);
	if(n){
		pEventLoop->removeEventListener(fd, EV_IO_ALL);
		close(fd);
		LOG_ERR("[worker]broken pipe");
		return ;
	}
	
	switch(ch.type){
		case CH_PIPE:
			LOG("channel msg pipefd");
			break;
		case CH_RELOAD:
			LOG("channel msg reload");
			break;
		case CH_EXIT:
			LOG("channel msg exit");
			break;
	}
}

void Worker::onConnection(int fd, int ev, void *data)
{
	//check maxclients
	Config *pConfig = Config::getInstance();
	if(pMaster->pGlobals->clients >= pConfig->nMaxClients){
		LOG("Connection is full");
		close(fd);
		return ;
	}
	
	//ip and port
	struct sockaddr_in *pAddr = (struct sockaddr_in*)data;
	char ip[17]; 
	int port = ntohs(pAddr->sin_port);
	memset(ip, 0, sizeof(ip));
	inet_ntop(AF_INET, &pAddr->sin_addr, ip, 16);
	
	//create client
	createClient(fd, ip, port);
	
	//free accept lock
	if(pMaster->bAcceptLock && 
		UnLockAcceptMutex(&pMaster->pGlobals->lock, nPid))
	{
		LOG("unlock accept mutex");
		pServer->stop();
	}
}

void Worker::onClose(Connection *client)
{
	closeClient(client);
}

void Worker::onMessage(Connection *client)
{
	Buffer *pBuffer = client->getBuffer();
	ClientData *pData = (ClientData*)client->getData();
	
	LOG("client message, fd=%d, len=%d", client->getSocket().getFd(), pBuffer->size());
	
	while(pBuffer->size())
	{
		bool proc = false;
		Buffer *msg = new Buffer();
		
		//自定义协议处理
		if(pMaster->pScript && pMaster->pScript->hasInputProc()){
			int ret = pMaster->pScript->procInput(client, pData->nrequest, pBuffer, msg);
			if(ret < 0){
				delete msg;
				return ;
			}else if(ret > 0){
				proc = true;
			}
		}
		
		//原生协议处理
		if(!proc){
			//check head
			size_t hdrSize = 4;
			if(pBuffer->size() < hdrSize){
				LOG("message head not enough");
				delete msg;
				return ;
			}
			
			//check body
			size_t msgSize = net::readNetInt32(pBuffer->data());
			if(pBuffer->size() < hdrSize + msgSize){
				LOG("message body size not enough, msgSize=%d, buffSize=%d", msgSize, pBuffer->size());
				delete msg;
				return ;
			}
			
			msg->append(pBuffer->data() + hdrSize, msgSize);
			
			pBuffer->seek(hdrSize + msgSize);
		}
	
		pData->nrequest++;
		LOG("process message, fd=%d, reqid=%d, proc=%d, buffer size=%d, msg len=%d", 
			client->getSocket().getFd(), 
			pData->nrequest,
			proc, 
			pBuffer->size(),
			msg->size());
	
		//create request
		if(!msg->empty()){
			Connection *backend = createBackendRequest(client, msg, &pData->params);
			if(backend){
				pData->backends[backend] = 1;
			}else{
				LOG_ERR("not found backend server");
			}
		}
	}
}

Connection* Worker::createBackendRequest(Connection *client, Buffer *request, Buffer *params, bool dParam)
{
	Connection *backend = NULL;
	Config *pConfig = Config::getInstance();
	int r = utils::randInt(1, pConfig->nMaxBackendWeights);
	int servers = pConfig->arrBackendServer.size();
	for(int i = 0; i < servers; ++i){
		BackendOption &opt = pConfig->arrBackendServer[i];
		r-= opt.weight;
		if(r <= 0 && (opt.type == SOCKET_TCP || opt.type == SOCKET_UNIX))
		{
			//创建连接
			backend = new Connection(pEventLoop);
			backend->setConnectHandler(EV_CB(this, Worker::onBackendConnect));
			backend->setMessageHandler(EV_CB(this, Worker::onBackendMessage));
			backend->setCloseHandler(EV_CB(this, Worker::onBackendClose));
			if(opt.type == SOCKET_TCP){
				LOG("backend connect to %s:%d", opt.host.c_str(), opt.port);
				backend->connectTcp(opt.host.c_str(), opt.port);
			}else{
				LOG("backend connect to unix:%s", opt.host.c_str());
				backend->connectUnix(opt.host.c_str());
			}
			
			//创建请求
			BackendRequest *pRequest = new BackendRequest();
			pRequest->client = client;
			pRequest->request = request;
			pRequest->dParam = dParam;
			pRequest->params = params;
			pRequest->connected = false;
			
			//绑定请求
			backend->setData(pRequest);
			
			//加入到超时列表
			if(pConfig->nBackendTimeout){
				arrExpireBackends[backend] = pConfig->nBackendTimeout;
			}
			
			break;
		}
	}
	
	return backend;
}

void Worker::onBackendConnect(Connection *backend)
{
	LOG("backend connected, conn=%p", backend);
	BackendRequest *pRequest = (BackendRequest*)backend->getData();
	Buffer bkRequest;
	const char *req = NULL;
	size_t len = 0;
	
	pRequest->connected = true;
	if(pRequest->request){
		req = pRequest->request->data();
		len = pRequest->request->size();
	}
	
	pProtocol->pack(req, len, bkRequest, pRequest->params);
	backend->send(bkRequest);
	
	arrExpireBackends.erase(backend);
}

void Worker::onBackendMessage(Connection *backend)
{
	LOG("backend data");
	BackendRequest *pRequest = (BackendRequest*)backend->getData();
	Buffer resp;
	Buffer *pBuffer = backend->getBuffer();
	int n = pProtocol->unpack(pBuffer->data(), pBuffer->size(), resp);
	if(n > 0){
		pBuffer->seek(n);
		if(pRequest->client && resp.size()){
			sendToClient(pRequest->client, &resp);
		}
	}
}

void Worker::onBackendClose(Connection *backend)
{
	LOG("backend close, conn=%p", backend);
	BackendRequest *pRequest = (BackendRequest*)backend->getData();
	Connection *pClient = pRequest->client;
	if(pClient){
		ClientData *pClientData = (ClientData*)pClient->getData();
		pClientData->backends.erase(backend);
	}
	
	if(!pRequest->connected){
		arrExpireBackends.erase(backend);
	}
	
	if(pRequest->dParam){
		delete pRequest->params;
	}
	
	if(pRequest->request){
		delete pRequest->request;
	}
	
	delete pRequest;
	delete backend;
}

void Worker::onBackendWriteComplete(Connection *backend)
{
	//LOG("backend write completed");
}

void Worker::onRouterMessage(Connection *router)
{
	Buffer *pBuffer = router->getBuffer();
	while(pBuffer->size())
	{
		if(pBuffer->size() < ROUTER_HEAD_SIZE){
			return ;
		}
		
		RouterMsg msg = Router::unpackMsg(pBuffer->data());
		if(pBuffer->size() < ROUTER_HEAD_SIZE + msg.slen + msg.len){
			return ;
		}
		
		RouterMsg *pMsg = (RouterMsg*)pBuffer->data();
		pMsg->type = msg.type;
		pMsg->slen = msg.slen;
		pMsg->len = msg.len;
		LOG("on router data,type=%d, slen=%d, len=%d", pMsg->type, pMsg->slen, pMsg->len);
		
		switch(pMsg->type){
			case ROUTER_MSG_KICK:
				doKick(pMsg);
				break;
			case ROUTER_MSG_SEND_MSG:
				doSendMsg(pMsg);
				break;
			case ROUTER_MSG_SEND_ALL:
				doSendAllMsg(pMsg);
				break;
			case ROUTER_MSG_CH_ADD:
				doChannelAdd(pMsg);
				break;
			case ROUTER_MSG_CH_DEL:
				doChannelDel(pMsg);
				break;
			case ROUTER_MSG_CH_PUB:
				doChannelPub(pMsg);
				break;
			default:
				LOG("invalid type");
				break;
		}
		
		pBuffer->seek(ROUTER_HEAD_SIZE + pMsg->slen + pMsg->len);
	}
}

void Worker::onRouterClose(Connection *router)
{
	LOG_ERR("router close");
	delete pRouter;
	pRouter = NULL;
	
	//下一次连接的时间
	nRouterDelay = nRouterReconnect * 3;
	nRouterReconnect++;
}

void Worker::onRouterConnect(Connection *router)
{
	LOG_INFO("router connected");
	
	nRouterReconnect = 0;
	nRouterDelay = 0;
	
	//AUTH
	Config *pConfig = Config::getInstance();
	char id[10];
	net::writeNetInt32(id, pConfig->nServerId);
	sendToRouter(ROUTER_MSG_AUTH, 0, NULL, sizeof(int), id);
	
	//sync session
	int sidnum = 0;
	std::string sidlist;
	for(ClientList::const_iterator it = arrClients.begin(); it != arrClients.end(); ++it){
		const std::string &sid = it->first;
		sidnum++;
		sidlist.append(sid);
	}
	if(sidnum){
		sendToRouter(ROUTER_MSG_CONN, sidnum * SID_LENGTH, sidlist.c_str(), 0, NULL);
	}
	
	//sync channel
	for(ChannelList::const_iterator it = arrChannels.begin(); it != arrChannels.end(); ++it){
		const std::string &cname = it->first;
		sendToRouter(ROUTER_MSG_CH_SUB, cname.size(), cname.c_str(), 0, NULL);
	}
}

void Worker::doKick(RouterMsg *pMsg)
{
	int pos = 0;
	while(pos + SID_LENGTH <= pMsg->slen)
	{
		std::string sid(pMsg->data + pos, SID_LENGTH);
		LOG("kick user, sid=%s", sid.c_str());
		ClientList::const_iterator it = arrClients.find(sid);
		if(it != arrClients.end()){
			Connection *pClient = it->second;
			pClient->close();
		}
		
		pos+= SID_LENGTH;
	}
}

void Worker::doSendMsg(RouterMsg *pMsg)
{
	int pos = 0;
	while(pos + SID_LENGTH <= pMsg->slen)
	{
		std::string sid(pMsg->data + pos, SID_LENGTH);
		ClientList::const_iterator it = arrClients.find(sid);
		if(it != arrClients.end()){
			Connection *pClient = it->second;
			LOG("send msg, sid=%s", sid.c_str());
			sendToClient(pClient, pMsg->data + pMsg->slen, pMsg->len);
		}else{
			LOG("not found client, sid=%s", sid.c_str());
		}
		
		pos+= SID_LENGTH;
	}
}

void Worker::doSendAllMsg(RouterMsg *pMsg)
{
	ClientList::const_iterator it;
	int n = 0;
	for(it = arrClients.begin(); it != arrClients.end(); ++it){
		Connection *pClient = it->second;
		sendToClient(pClient, pMsg->data, pMsg->len);
		n++;
	}
	
	LOG("sendall msg n=%d", n);
}

void Worker::doChannelAdd(RouterMsg *pMsg)
{
	//频道名称在data域
	int pos = 0;
	std::string chname(pMsg->data + pMsg->slen, pMsg->len);
	ConnectionSet &clients = arrChannels[chname];
	while(pos + SID_LENGTH <= pMsg->slen)
	{
		std::string sid(pMsg->data + pos, SID_LENGTH);
		ClientList::const_iterator it = arrClients.find(sid);
		if(it != arrClients.end()){
			Connection *pClient = it->second;
			ClientData *pClientData = (ClientData*)pClient->getData();
			pClientData->channels[chname] = 1;
			
			clients[pClient] = 1;
			if(clients.size() == 1){
				sendToRouter(ROUTER_MSG_CH_SUB, chname.size(), chname.c_str(), 0, NULL);
			}
			
			LOG("join channel name=%s, sid=%s", chname.c_str(), sid.c_str());
		}
		
		pos+= SID_LENGTH;
	}
}

void Worker::doChannelDel(RouterMsg *pMsg)
{
	//频道名称在data域
	int pos = 0;
	std::string chname(pMsg->data + pMsg->slen, pMsg->len);
	ConnectionSet &clients = arrChannels[chname];
	while(pos + SID_LENGTH <= pMsg->slen)
	{
		std::string sid(pMsg->data + pos, SID_LENGTH);
		ClientList::const_iterator it = arrClients.find(sid);
		if(it != arrClients.end()){
			Connection *pClient = it->second;
			ClientData *pClientData = (ClientData*)pClient->getData();
			pClientData->channels.erase(chname);
			
			clients.erase(pClient);
			if(!clients.size()){
				sendToRouter(ROUTER_MSG_CH_UNSUB, chname.size(), chname.c_str(), 0, NULL);
			}
			
			LOG("leave channel name=%s, sid=%s", chname.c_str(), sid.c_str());
		}
		
		pos+= SID_LENGTH;
	}
}

void Worker::doChannelPub(RouterMsg *pMsg)
{
	//频道名称在session域
	std::string chname(pMsg->data, pMsg->slen);
	ChannelList::const_iterator it = arrChannels.find(chname);
	if(it == arrChannels.end()){
		return ;
	}
	
	const ConnectionSet &clients = it->second;
	ConnectionSet::const_iterator it2;
	for(it2 = clients.begin(); it2 != clients.end(); ++it2){
		Connection *pClient = it2->first;
		ClientData *pClientData = (ClientData*)pClient->getData();
		sendToClient(pClient, pMsg->data + pMsg->slen, pMsg->len);
		
		LOG("send channel msg to: %s", pClientData->session.getId());
	}
}

void Worker::loopBefore(void *data)
{
	if(pMaster->bAcceptLock && 
		pMaster->pGlobals->workerClients[nId] < nPerWorkerAcceptMax &&
		LockAcceptMutex(&pMaster->pGlobals->lock, nPid))
	{
		LOG("lock accept mutex");
		pServer->start();
	}
}

void Worker::onTimer(long long id, void *data)
{
	//router check
	if(!pRouter && --nRouterDelay <= 0){
		initRouter();
	}
	
	//timeout check
	for(BackendList::iterator it = arrExpireBackends.begin();
		it != arrExpireBackends.end(); ++it)
	{
		if(--it->second <= 0){
			LOG("backend timeout, conn=%p", it->first);
			it->first->close();
		}
	}
}

void Worker::initRouter()
{
	//connect router
	Config *pConfig = Config::getInstance();
	pRouter = new Connection(pEventLoop);
	pRouter->setConnectHandler(EV_CB(this, Worker::onRouterConnect));
	pRouter->setMessageHandler(EV_CB(this, Worker::onRouterMessage));
	pRouter->setCloseHandler(EV_CB(this, Worker::onRouterClose));
	pRouter->connectTcp(pConfig->sRouterHost.c_str(), pConfig->nRouterPort);
	LOG_INFO("connect router server %s:%d", 
		pConfig->sRouterHost.c_str(), 
		pConfig->nRouterPort);
}

void Worker::proc()
{
	//还原信号默认处理方式
	signal(SIGTERM, NULL);
	signal(SIGUSR1, NULL);

	//set process title
	Config *pConfig = Config::getInstance();
	char title[256];
	snprintf(title, 256, "fooking gateway worker, fooking %s", pConfig->sFile.c_str());
	utils::setProcTitle(pMaster->pArgv, title);
	
	//每个worker最大允许的连接数
	nPerWorkerAcceptMax = static_cast<int>(pConfig->nMaxClients / pConfig->nWorkers * 1.05);
	
	//create loop
	pEventLoop = new EventLoop();
	pEventLoop->setTimer(1000, EV_TIMER_CB(this, Worker::onTimer), NULL);
	pEventLoop->addEventListener(nPipefd, EV_IO_READ, EV_IO_CB(this, Worker::onChannel), NULL);
	pEventLoop->setLoopBefore(EV_CB(this, Worker::loopBefore), NULL);
	
	//listen server
	pServer = new Server(pEventLoop, pMaster->pServer->getSocket().getFd());
	pServer->setConnectionHandler(EV_IO_CB(this, Worker::onConnection));
	if(!pMaster->bAcceptLock){
		pServer->start();
	}
	
	//init router
	initRouter();
	
	LOG("worker started, pipefd=%d", nPipefd);
	pEventLoop->run();
}